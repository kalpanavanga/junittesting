package com.innominds.kalpana.junit_practice;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateTime {

	public String currentDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("13-09-2022 'at' 10:33:12 z");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }



   public String add10HoursToDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("13-09-2022 'at' 10:33:12 z");
        Date oldDate = new Date(System.currentTimeMillis());
        Date newDate = new Date(oldDate.getTime() + TimeUnit.HOURS.toMillis(10));
        return formatter.format(newDate);
    }


}
