package com.innominds.kalpana.junit_practice;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.InflaterInputStream;

public class Decompress {

	public static void main(String[] args) throws IOException {
	}

	void decompressFile(String fileToDeCompress, String DecompressFile) throws IOException {
		try (FileInputStream fin = new FileInputStream(fileToDeCompress);
				InflaterInputStream in = new InflaterInputStream(fin);
				FileOutputStream fout = new FileOutputStream(DecompressFile);) {
			int i;
			while ((i = in.read()) != -1) {
				fout.write((byte) i);
				fout.flush();

			}
		}
		System.out.println("Decompression is done...");

	}

}
