package com.innominds.kalpana.junit_practice;

public class Question2 {
	
	public static  boolean StringTest(String input) {
		if(isUpper(input)) {
			return true;
		}
		else if(isLower(input)) {
			return true;
		}
		else if(isNumber(input)) {
			return true;
		}
		else {
			return true;
		}
	}
	public static boolean isUpper(String input) {
		for(int i=0;i<input.length();i++) {
			if(!(input.charAt(i)<='Z' && input.charAt(i)>='A')) {
				return false;
			}
		}
		return true;
	}
	public static boolean isLower(String input) {
		for(int i=0;i<input.length();i++) {
			if(!(input.charAt(i)<='z' && input.charAt(i)>='a')) {
				return false;
			}
		}
		return true;
	}
	public static boolean isNumber(String input) {
		for(int i=0;i<input.length();i++) {
			if(!(input.charAt(i)<='9' && input.charAt(i)>='0')) {
				return false;
			}
		}
		return true;
	}
	
	public static void main(String[] args) {
		
	}
}
