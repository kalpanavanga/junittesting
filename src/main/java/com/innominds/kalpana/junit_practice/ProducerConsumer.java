package com.innominds.kalpana.junit_practice;

import java.util.LinkedList;

public class ProducerConsumer {

	LinkedList<Integer> list = new LinkedList();

	public void Produce() throws InterruptedException {
		int value = 0;
		while (true) {
			synchronized (this) {

				while (list.size() > 0)
					wait();

				System.out.println("Producer data" + value);
				list.add(value);
				value++;
				notify();
				Thread.sleep(1000);
			}
		}
	}

	public void Consumer() throws InterruptedException {

		while (true) {
			synchronized (this) {
				while (list.size() == 0) {
					wait();
					int value = list.removeFirst();

					System.out.println("consumer data" + value);

					notify();
					Thread.sleep(1000);
				}
			}
		}
	}

}
