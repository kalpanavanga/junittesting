package com.innominds.kalpana.junit_practice;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Jdbc {

	static final String DB_URL = "jdbc:mysql://localhost/springdb";
	static final String USER = "root";
	static final String PASS = "root";

	public boolean DMLCreateDatabase() {

		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {
			String sql = "create database springdb";
			stmt.executeUpdate(sql);
			System.out.println("Database created successfully...");
			return true;// database successfully created

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;// database creation fails
	}

	public boolean DMLCountTables() {
		try (Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
				Statement stmt = conn.createStatement();) {
			String sql = "select found_rows()";
			stmt.execute(sql);
			System.out.println("Conut the Database successfully...");
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static void main(String[] args) {
	}

}
