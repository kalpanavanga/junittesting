package com.innominds.kalpana.junit_practice;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class DaemonThreadTest {

	@Test
	void testdaemon() {
		DaemonThread t1 = new DaemonThread();
		t1.setDaemon(true);
		assertEquals(false, DaemonThread.currentThread().isDaemon());
		assertEquals(true, t1.isDaemon());

	}

}
