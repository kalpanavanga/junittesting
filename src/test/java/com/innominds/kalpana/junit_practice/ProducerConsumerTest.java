package com.innominds.kalpana.junit_practice;

import org.junit.jupiter.api.Test;

class ProducerConsumerTest {

	@Test
	void test() throws InterruptedException {
		ProducerConsumer pc = new ProducerConsumer();
		Thread t1 = new Thread(new Runnable() {

			public void run() {
				try {
					pc.Produce();

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		Thread t2 = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					pc.Consumer();

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		t1.start();
		t2.start();

		t1.join();
		t2.join();

	}

}
