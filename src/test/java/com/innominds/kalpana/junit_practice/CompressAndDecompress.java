package com.innominds.kalpana.junit_practice;

import java.io.IOException;

import org.junit.jupiter.api.Test;

class CompressAndDecompress {

	@Test
	void test() throws IOException {

		Compress compress = new Compress();
		compress.compressFile("C:\\Users\\kvanga\\HelloWorld.java", "C:\\Users\\kvanga\\compressed.txt");
	}

	@Test
	void test1() throws IOException {
		Decompress decompress = new Decompress();
		decompress.decompressFile("C:\\Users\\kvanga\\compressed.txt", "C:\\Users\\kvanga\\HelloWorld.java");

	}

}
