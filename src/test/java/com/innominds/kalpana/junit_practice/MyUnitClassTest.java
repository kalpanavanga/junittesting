package com.innominds.kalpana.junit_practice;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MyUnitClassTest {

	@Test
	void nonstatictest() {
		MyUnitClass obj1 = new MyUnitClass();
		MyUnitClass obj2 = new MyUnitClass();
		MyUnitClass obj3 = new MyUnitClass();
		System.out.println(obj1.nonstaticcount);
		assertNotEquals(3, obj1.nonstaticcount);

	}

	@Test
	void statictest() {
		MyUnitClass obj4 = new MyUnitClass();
		System.out.println(obj4.countstatic);
		MyUnitClass obj5 = new MyUnitClass();
		System.out.println(obj5.countstatic);
		MyUnitClass obj6 = new MyUnitClass();
		System.out.println(obj6.countstatic);
		assertEquals(6, obj6.countstatic);

	}
}
