package com.innominds.kalpana.junit_practice;

import org.junit.jupiter.api.Test;

class LambdaExpressionExampleTest {

	@Test
	void test() {
		Addition ad2 = (int a, int b) -> (a + b);
		System.out.println(ad2.add(50, 20));

	}
}
