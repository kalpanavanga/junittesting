package com.innominds.kalpana.junit_practice;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Scanner;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class Question2Test {

	String input;
	@BeforeEach
	public  void userInputMehtod() {
		Scanner scan=new Scanner(System.in);
		input=scan.next();
	}

	@Test
	void testforUppercase() {
		assertNotNull(input);
		assertEquals(true,Question2.isUpper(input));
	}
	@Test
	void testforLowercase() {
		assertNotNull(input);
		assertEquals(true,Question2.isLower(input));
	}
	@Test
	void testforNumbercase() {
		assertNotNull(input);
		assertEquals(true,Question2.isNumber(input));
	}

}
